from flask import Flask, render_template, request, redirect, session
from flask_mysqldb import MySQL

app = Flask(__name__)
app.secret_key = 'your_secret_key'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '1234'
app.config['MYSQL_DB'] = 'employee'

mysql = MySQL(app)


@app.route('/')
def index():
    return render_template('logdb.html')


@app.route('/login', methods=['POST'])
def login():
    username = request.form['username']
    password = request.form['password']

    cursor = mysql.connection.cursor()
    cursor.execute("SELECT * FROM users WHERE username = %s AND password = %s", (username, password))
    user = cursor.fetchone()

    if user:
        # Assuming the 'username' and 'password' columns exist in your table
        session['username'] = user[0]  # Access the username using index 1
        return redirect('/dashboard')
    else:
        return "Invalid username or password"


@app.route('/dashboard')
def dashboard():
    if 'username' in session:
        return f"Welcome, {session['username']}!!!"
    else:
        return redirect('/')


if __name__ == '__main__':
    app.run(debug=True)
