# app.py
import os
from flask import Flask, render_template, request, jsonify
import cv2
import numpy as np
from database import db_session, init_db, Person

app = Flask(__name__)

# Configure database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
init_db(app)

# Folder to store uploaded images
UPLOAD_FOLDER = 'static/uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Route for uploading person information and image
@app.route('/upload', methods=['POST'])
def upload():
    name = request.form['name']
    image_file = request.files['image']

    if image_file and allowed_file(image_file.filename):
        image_path = os.path.join(app.config['UPLOAD_FOLDER'], image_file.filename)
        image_file.save(image_path)

        # Save person information in the database
        person = Person(name=name, image_path=image_path)
        db_session.add(person)
        db_session.commit()

        return jsonify({'message': 'Upload successful'})

    return jsonify({'error': 'Invalid file'})

# Route for recognizing the person from webcam image
@app.route('/recognize', methods=['POST'])
def recognize():
    image_file = request.files['image']

    if image_file and allowed_file(image_file.filename):
        image_path = os.path.join(app.config['UPLOAD_FOLDER'], 'temp_image.jpg')
        image_file.save(image_path)

        # Process the image and recognize the person
        recognized_person = recognize_person(image_path)

        if recognized_person:
            return jsonify(recognized_person)
        else:
            return jsonify({'error': 'Person not found'})

    return jsonify({'error': 'Invalid file'})

# Helper function to check if the file is allowed
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in {'jpg', 'jpeg', 'png'}

# Helper function for recognizing the person from the image
def recognize_person(image_path):
    # Implement your image recognition logic here
    # This can involve using face recognition or other AI-based methods
    # For simplicity, let's assume the person is recognized based on the file name

    image_name = os.path.basename(image_path)
    person = Person.query.filter_by(image_path=image_name).first()

    if person:
        return {'name': person.name}

    return None

if __name__ == '__main__':
    app.run(debug=True)
