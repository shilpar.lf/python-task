import nltk
import random
from nltk.corpus import movie_reviews
from nltk.tokenize import word_tokenize
from sklearn.svm import SVC
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


# nltk.download('movie_reviews')

positive_reviews = [' '.join(movie_reviews.words(fileid)) for fileid in movie_reviews.fileids('pos')]
# print(positive_reviews)
negative_reviews = [' '.join(movie_reviews.words(fileid)) for fileid in movie_reviews.fileids('neg')]
all_reviews = positive_reviews + negative_reviews

labels = ['positive'] * len(positive_reviews) + ['negative'] * len(negative_reviews)

vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(all_reviews)

X_train, X_test, y_train, y_test = train_test_split(X, labels, test_size=0.2, random_state=42)
svm_classifier = SVC(kernel='linear')
svm_classifier.fit(X_train, y_train)

y_pred = svm_classifier.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
print("Accuracy:", accuracy)

def predict_sentiment(text):
    text_vectorized = vectorizer.transform([text])
    sentiment = svm_classifier.predict(text_vectorized)[0]
    return sentiment

text_to_predict = input("Enter your Command: ")
sentiment = predict_sentiment(text_to_predict)
print("Predicted Sentiment:", sentiment)
