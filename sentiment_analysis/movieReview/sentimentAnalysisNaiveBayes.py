import nltk
#USE POSTAG
from nltk.tag import pos_tag
# IMPORT "twitter_samples" DATASET
from nltk.corpus import movie_reviews
#word lemmatization
from nltk.stem.wordnet import WordNetLemmatizer
import re, string
from nltk.corpus import stopwords
from nltk import FreqDist
import random
# Accuracy
from nltk import classify
from nltk import NaiveBayesClassifier
# CHECK MODEL PERFORMANCE
from nltk.tokenize import word_tokenize

# EXTRACT THE POSITIVE, NEGATIVE AND NUTRAL DATASET

positive_tweets = movie_reviews.raw('positive')
negative_tweets = movie_reviews.raw('negative')
# text = movie_reviews.raw('tweets.20150430-223406.json')

#TOKENIZE THE DATASETS
tweet_tokens = movie_reviews.word_tokenize('positive_reviews.json')
positive_tweet_tokens = movie_reviews.word_tokenize('positive_reviews.json')
negative_tweet_tokens = movie_reviews.word_tokenize('negative_eviews.json')

# stop_words = stopwords.words('english')
# print(remove_noise(tweet_tokens[0], stop_words))


positive_cleaned_tokens_list = []
negative_cleaned_tokens_list = []

#USING LEMMATIZATION TO REMOVING NOISE LIKE URLS(http,https) AND USERNAME
def remove_noise(tweet_tokens, stop_words=()):
    cleaned_tokens = []

    for token, tag in pos_tag(tweet_tokens):
        token = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+#]|[!*\(\),]|' \
                       '(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', token)
        token = re.sub("(@[A-Za-z0-9_]+)", "", token)

        if tag.startswith("NN"):
            pos = 'n'
        elif tag.startswith('VB'):
            pos = 'v'
        else:
            pos = 'a'

        lemmatizer = WordNetLemmatizer()
        token = lemmatizer.lemmatize(token, pos)

        if len(token) > 0 and token not in string.punctuation and token.lower() not in stop_words:
            cleaned_tokens.append(token.lower())
    return cleaned_tokens


stop_words = stopwords.words('english')
# print(remove_noise(tweet_tokens[0], stop_words))
for tokens in positive_tweet_tokens:
    positive_cleaned_tokens_list.append(remove_noise(tokens, stop_words))

for tokens in negative_tweet_tokens:
    negative_cleaned_tokens_list.append(remove_noise(tokens, stop_words))


# print(remove_noise(tweet_tokens[0], stop_words))
# print(positive_tweet_tokens[500])
# print(positive_cleaned_tokens_list[500])

#Determine word density that is how many time that word is occurres in that data set
def get_all_words(positive_cleaned_tokens):
    for tokens in positive_cleaned_tokens:
        for token in tokens:
            yield token


all_pos_words = get_all_words(positive_cleaned_tokens_list)
freq_dist_pos = FreqDist(all_pos_words)
print(freq_dist_pos.most_common(10))

#Converting Tokens to a Dictionary
def get_tweets_for_model(cleaned_tokens_list):
    for tweet_tokens in cleaned_tokens_list:
        yield dict([token, True] for token in tweet_tokens)



#POST TAG OF THE INDEX 0 TH POSITIVE
tweet_tokens = movie_reviews.tokenized('positive_tweets.json')
print(pos_tag(tweet_tokens[0]))


positive_tokens_for_model = get_tweets_for_model(positive_cleaned_tokens_list)
negative_tokens_for_model = get_tweets_for_model(negative_cleaned_tokens_list)

positive_dataset = [(tweet_dict, "Positive")
                    for tweet_dict in positive_tokens_for_model]
# print(positive_dataset)
negative_dataset = [(tweet_dict, "Negative")
                    for tweet_dict in negative_tokens_for_model]
# print(positive_dataset)
dataset = positive_dataset + negative_dataset

random.shuffle(dataset)

train_data = dataset[:7000]
test_data = dataset[7000:]

# ACCURACY
classifier = NaiveBayesClassifier.train(train_data)
accuracy = classify.accuracy(classifier, test_data)
print("Accuracy is:", accuracy * 100, "%")

custom_tweet = input("Enter feed back: ")

custom_tokens = remove_noise(word_tokenize(custom_tweet))

print(classifier.classify(dict([token, True] for token in custom_tokens)))

# print(classifier.classify(custom_tokens))