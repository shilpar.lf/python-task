import numpy as np # linear algebra
import pandas as pd # data processing
pd.options.mode.chained_assignment = None
import os #File location

from nltk.corpus import stopwords

from wordcloud import WordCloud #Word visualization
import matplotlib.pyplot as plt #Plotting properties
import seaborn as sns #Plotting properties
from sklearn.feature_extraction.text import CountVectorizer #Data transformation
from sklearn.model_selection import train_test_split #Data testing
from sklearn.linear_model import LogisticRegression #Prediction Model
from sklearn.metrics import accuracy_score,precision_score,recall_score,f1_score #Comparison between real and predicted
from xgboost import XGBClassifier
from sklearn.preprocessing import LabelEncoder #Variable encoding and decoding for XGBoost
import re #Regular expressions
import nltk
from nltk import word_tokenize
# nltk.download('stopwords')


csv_path = r'C:\Users\admin\PycharmProjects\pythonTask\sentiment_analysis\csvFiles\tweet.csv'

df = pd.read_csv(csv_path, delimiter=',', encoding='ISO-8859-1')
df.columns=['id','label','tweet']
print(df.head())
train_data=df
print(train_data)
#Text transformation
train_data["lower"]=train_data.tweet.str.lower() #lowercase
train_data["lower"]=[str(data) for data in train_data.lower] #converting all to string
train_data["lower"]=train_data.lower.apply(lambda x: re.sub('[^A-Za-z0-9 ]+', ' ', x)) #regex
print(train_data.head())


# 2. Plotting features
word_cloud_text = ''.join(train_data[train_data["label"]==0].lower)
# Creation of wordcloud
wordcloud = WordCloud(
    max_font_size=100,
    max_words=100,
    background_color="black",
    scale=10,
    width=800,
    height=800
).generate(word_cloud_text)
#Figure properties
plt.figure(figsize=(10,10))
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.title('positive data')

# plt.show()

word_cloud_text = ''.join(train_data[train_data["label"]==1].lower)
#Creation of wordcloud
wordcloud = WordCloud(
    max_font_size=100,
    max_words=100,
    background_color="black",
    scale=10,
    width=800,
    height=800
).generate(word_cloud_text)
#Figure properties
plt.figure(figsize=(10,10))
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.title('Negative data')

# plt.show()

# plot1=df.groupby(by=["id","label"]).count().reset_index()
# print(plot1.head())
#Text splitting
tokens_text = [word_tokenize(str(word)) for word in train_data.lower]
#Unique word counter
tokens_counter = [item for sublist in tokens_text for item in sublist]
print("Number of tokens: ", len(set(tokens_counter)))

# print(tokens_text[1])

#Choosing english stopwords
stopwords_nltk = nltk.corpus.stopwords
stop_words = stopwords_nltk.words('english')
print(stop_words[:5])

# Logistic Regression model
#Initial Bag of Words
bow_counts = CountVectorizer(
    tokenizer=word_tokenize,
    stop_words=stop_words, #English Stopwords
    ngram_range=(1, 1) #analysis of one word
)

#Train - Test splitting
reviews_train, reviews_test = train_test_split(train_data, test_size=0.2, random_state=0)

#Creation of encoding related to train dataset
X_train_bow = bow_counts.fit_transform(reviews_train.lower)
#Transformation of test dataset with train encoding
X_test_bow = bow_counts.transform(reviews_test.lower)

# print(X_test_bow)

#Labels for train and test encoding
y_train_bow = reviews_train['label']
y_test_bow = reviews_test['label']

#Total of registers per category
cat = y_test_bow.value_counts()/ y_test_bow.shape[0]
print(cat)

# Logistic regression
model1 = LogisticRegression(C=1, solver="liblinear",max_iter=200)
model1.fit(X_train_bow, y_train_bow)
# Prediction
test_pred = model1.predict(X_test_bow)
print("Accuracy: ", accuracy_score(y_test_bow, test_pred) * 100)

print("precision: ", precision_score(y_test_bow, test_pred) * 100)

print("Recall: ", recall_score(y_test_bow, test_pred) * 100)

print("f1_score: ", f1_score(y_test_bow, test_pred) * 100)

new_input = "This is a great movie, I loved it!"
new_input_cleaned = re.sub('[^a-zA-Z]', ' ', new_input)
new_input_cleaned = new_input_cleaned.lower()
new_input_tokens = word_tokenize(new_input_cleaned)

# Transform the input using the same CountVectorizer
new_input_bow = bow_counts.transform([new_input_cleaned])

# Make prediction
predicted_label = model1.predict(new_input_bow)

if predicted_label[0] == 0:
    sentiment = "positive"
else:
    sentiment = "negative"

print("Predicted Sentiment:", sentiment)