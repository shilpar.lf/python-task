import pandas as pd
import numpy as np
import re
import nltk
# nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

# Step 1 − Collecting the Dataset
csv_path = r'C:\Users\admin\PycharmProjects\pythonTask\sentiment_analysis\csvFiles\less_sen.csv'
# csv_path=r'C:\Users\admin\PycharmProjects\pythonTask\sentiment_analysis\csvFiles\sentiment.csv'
df = pd.read_csv(csv_path, delimiter=',', encoding='ISO-8859-1', low_memory=False)
df.columns = ['ItemID', 'Sentiment', 'SentimentText']
df = df[['Sentiment', 'SentimentText']]
col = df.columns
print(col)

val_count = df.Sentiment.value_counts()
print(val_count)

corpus = []
stemmer = PorterStemmer()
for i in range(0, len(df)):
    review = re.sub('[^a-zA-Z]', ' ', df['SentimentText'][i])
    review = review.lower()
    review = review.split()
    review = [stemmer.stem(word) for word in review if word not in set(stopwords.words('english'))]
    review = ' '.join(review)
    corpus.append(review)
    # print(corpus)

# Step 3− Creating the TF-IDF Matrix
# vectorizer = TfidfVectorizer(max_features=15000)
vectorizer = TfidfVectorizer(max_features=500)  # Use a lower number of features

print(vectorizer)
X = vectorizer.fit_transform(corpus).toarray()
y = df.iloc[:, 1].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

model = LogisticRegression()
model.fit(X_train, y_train)

# Step 6− Evaluating the Model
y_pred = model.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
precision = precision_score(y_test, y_pred, average='weighted', zero_division=1)
recall = recall_score(y_test, y_pred, average='weighted', zero_division=1)
f1 = f1_score(y_test, y_pred, average='weighted')
print(f"Accuracy: {accuracy}")
print(f"Precision: {precision}")
print(f"Recall: {recall}")
print(f"F1 score: {f1}")

