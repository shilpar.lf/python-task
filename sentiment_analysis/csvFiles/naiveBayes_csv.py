import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from nltk.corpus import stopwords
from string import punctuation
from nltk.tokenize import word_tokenize
from nltk.stem import LancasterStemmer
from string import punctuation
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import LancasterStemmer
from nltk.stem.wordnet import WordNetLemmatizer
import re
import warnings
from sklearn.utils import resample
from wordcloud import WordCloud
import seaborn as sns
import random
#accuracy
from nltk import classify
from nltk import NaiveBayesClassifier

# %%time
warnings.filterwarnings('ignore')

csv_path = r'C:\Users\admin\PycharmProjects\pythonTask\sentiment_analysis\csvFiles\sentiment.csv'

df = pd.read_csv(csv_path, delimiter=',', encoding='ISO-8859-1')
df.columns = ['ItemID', 'Sentiment', 'SentimentText']
top = df.head()
print(top)

df = df[['Sentiment', 'SentimentText']]
col = df.columns
print(col)

val_count = df.Sentiment.value_counts()
print(val_count)

# df['Sentiment'] = df['Sentiment'].replace({1:4})

# sns.countplot(df["Sentiment"])
sns.countplot(data=df, x="Sentiment", hue="Sentiment")

plt.title("Count Plot of Sentiment")
plt.show()

Null = df.isna().sum().sum()
# print(null)
## majority class 0
df_majority = df[df['Sentiment'] == 0]
## minority class 1
df_minority = df[df['Sentiment'] == 1]
shape1 = df_minority.shape
print(shape1)
shape2 = df_majority.shape
print(shape2)

# replace if minarity and mejarity has more difference
df_minority_downsampled = resample(df_minority,
                                   replace=True,
                                   n_samples=len(df_majority),
                                   random_state=1234)

# df = df_majority_downsampled.append(df_majority)
# head1=df.head()
# print(head1)
df_upsampled = pd.concat([df_minority_downsampled, df_majority])

head1 = df_upsampled.head()
print(head1)
df.dropna(subset=['SentimentText'], inplace=True)

# Ensure the 'SentimentText' column is of string type
df['SentimentText'] = df['SentimentText'].astype(str)
# sns.countplot(df["Sentiment"])
sns.countplot(data=df, x="Sentiment", hue="Sentiment")

plt.title("Count Plot of target class")
# plt.show()
stuff_to_be_removed = list(stopwords.words('english')) + list(punctuation)
stemmer = LancasterStemmer()

corpus = df['SentimentText'].tolist()
print(len(corpus))
print(corpus[0])

final_corpus = []
final_corpus_joined = []
for i in df.index:
    text = re.sub('[^a-zA-Z]', ' ', df['SentimentText'][i])
    # Convert to lowercase
    text = text.lower()
    # remove tags
    text = re.sub("&lt;/?.*?&gt;", " &lt;&gt; ", text)

    # remove special characters and digits
    text = re.sub("(\\d|\\W)+", " ", text)

    ##Convert to list from string
    text = text.split()

    # Lemmatisation
    lem = WordNetLemmatizer()
    text = [lem.lemmatize(word) for word in text
            if not word in stuff_to_be_removed]
    text1 = " ".join(text)
    final_corpus.append(text)
    final_corpus_joined.append(text1)

data_cleaned = pd.DataFrame()
data_cleaned["SentimentText"] = final_corpus_joined
data_cleaned["Sentiment"] = df["Sentiment"].values
data_cleaned['Sentiment'].value_counts()
cleaned = data_cleaned.head()
print(cleaned)

#EDA
data_eda = pd.DataFrame()
data_eda['SentimentText'] = final_corpus
data_eda['Sentiment'] = df["Sentiment"].values
eda1=data_eda.head()
print(eda1)

# Storing positive data seperately
positive = data_eda[data_eda['Sentiment'] == 1]
positive_list = positive['SentimentText'].tolist()

# Storing negative data seperately

negative = data_eda[data_eda['Sentiment'] == 0]
negative_list = negative['SentimentText'].tolist()

# positive_all = " ".join([word for sent in positive_list for word in sent ])
# negative_all = " ".join([word for sent in negative_list for word in sent ])
#
# WordCloud()
# wordcloud = WordCloud(width=500,
#                       height=500,
#                       background_color='#F2EDD7FF',
#                       max_words = 100).generate(negative_all)
#
# plt.figure(figsize=(20,30))
# plt.imshow(wordcloud)
# plt.title("Negative")
# plt.show()
# def get_count(data):
#     dic = {}
#     for i in data:
#         for j in i:
#             if j not in dic:
#                 dic[j] = 1
#             else:
#                 dic[j] += 1
#
#     return (dic)
#
#
# count_corpus = get_count(negative_list)
#
# count_corpus = pd.DataFrame({"word":count_corpus.keys(),"count":count_corpus.values()})
# count_corpus = count_corpus.sort_values(by = "count", ascending = False)
#
# plt.figure(figsize = (15,10))
# sns.barplot(x = count_corpus["word"][:20], y = count_corpus["count"][:20])
# plt.title('one words in positive data')
# plt.show()

#Naive bayes for sentiment analysis
def get_tweets_for_model(cleaned_tokens_list):
    for tweet_tokens in cleaned_tokens_list:
        yield dict([token, True] for token in tweet_tokens)

positive_tokens_for_model = get_tweets_for_model(positive_list)
negative_tokens_for_model = get_tweets_for_model(negative_list)


positive_dataset = [(review_dict, "Positive")
                     for review_dict in positive_tokens_for_model]

negative_dataset = [(review_dict, "Negative")
                     for review_dict in negative_tokens_for_model]
dataset = positive_dataset + negative_dataset

random.shuffle(dataset)

train_data = dataset[:333091]
test_data = dataset[333091:]

classifier = NaiveBayesClassifier.train(train_data)

print(" Training Accuracy is:", round(classify.accuracy(classifier, train_data),2)*100)

print("Testing Accuracy is:", round(classify.accuracy(classifier, test_data),2)*100)

print(classifier.show_most_informative_features(10))

new_input = "This is a great movie, I loved it!"
new_input_tokens = word_tokenize(new_input)
new_input_features = {token: True for token in new_input_tokens}

predicted_sentiment = classifier.classify(new_input_features)
print("Predicted Sentiment:", predicted_sentiment)

