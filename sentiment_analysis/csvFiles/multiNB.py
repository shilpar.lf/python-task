import pandas as pd
from nltk.corpus import wordnet
from nltk.stem import SnowballStemmer

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scikitplot as skplt
from sklearn.feature_extraction.text import CountVectorizer
from collections import Counter
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize, RegexpTokenizer, ToktokTokenizer
import re
from wordcloud import WordCloud
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, recall_score, precision_score,f1_score
from sklearn.naive_bayes import MultinomialNB
from string import punctuation
from nltk.stem import WordNetLemmatizer
import os

pd.set_option('display.float_format', lambda x: '%.3f' % x)

csv_path = r'C:\Users\admin\PycharmProjects\pythonTask\sentiment_analysis\csvFiles\sentiment.csv'
df = pd.read_csv(csv_path, delimiter=',', encoding='ISO-8859-1', low_memory=False)
# df=df.head(2)
# print(df)
df = df[['Sentiment', 'SentimentText']]
shape1 = df.shape
# print(shape1)


# fill the missing places
df['SentimentText'] = df['SentimentText'].fillna(' ')
df['Sentiment'] = df['Sentiment'].fillna(' ')

missing = df.isna().sum()
print(missing)

# Renaming Target Variable Values
df.loc[df["Sentiment"] == 0, "Sentiment"] = "negative"  # 0 -> Not Recommended
df.loc[df["Sentiment"] == 1, "Sentiment"] = "positive"  # 1 -> Recommended
typ = df.dtypes
print(typ)

# Check Proportion Target Class Variable
count_class = pd.value_counts(df["Sentiment"], sort=True)
count_class.plot(kind='bar', color=["blue", "orange"])
plt.title('Proportion Target Class')
plt.show()

# find positive and negative percentages
print('positive', round(df['Sentiment'].value_counts()['positive'] / len(df) * 100), '%')
print('negative', round(df['Sentiment'].value_counts()['negative'] / len(df) * 100), '%')

# Check Most Common Words in Each Target Variable Values - positive

count1 = Counter(" ".join(df[df['Sentiment'] == 'positive']['SentimentText']).
                 split()).most_common(20)
df1 = pd.DataFrame.from_dict(count1)
df1 = df1.rename(columns={0: "common_words", 1: "count"})
# print(df1.head(10))

# Check Most Common Words in Each Target Variable Values - Negative

count2 = Counter(" ".join(df[df['Sentiment'] == 'negative']['SentimentText']). \
                 split()).most_common(20)
df2 = pd.DataFrame.from_dict(count2)
df2 = df2.rename(columns={0: "common_words", 1: "count"})
# print(df2.head(10))

# print(df.iloc[1, 0])

# Text Mining - In this text mining process we will exploring and analyzing unstructured text data

tokenizer = ToktokTokenizer()
stopword_list = nltk.corpus.stopwords.words('english')
main_text = df['SentimentText']
target = df['Sentiment']

# print(len(main_text))
# print(len(target))

# Expanding Contraction
contractions_dict = {
    "ain't": "am not",
    "aren't": "are not",
    "can't": "cannot",
    "can't've": "cannot have",
    "'cause": "because",
    "could've": "could have",
    "couldn't": "could not",
    "couldn't've": "could not have",
    "didn't": "did not",
    "doesn't": "does not",
    "don't": "do not",
    "hadn't": "had not",
    "hadn't've": "had not have",
    "hasn't": "has not",
    "haven't": "have not",
    "he'd": "he had",
    "he'd've": "he would have",
    "he'll": "he will",
    "he'll've": "he will have",
    "he's": "he is",
    "how'd": "how did",
    "how'd'y": "how do you",
    "how'll": "how will",
    "how's": "how is",
    "I'd": "I had",
    "I'd've": "I would have",
    "I'll": "I will",
    "I'll've": "I will have",
    "I'm": "I am",
    "I've": "I have",
    "isn't": "is not",
    "it'd": "it had",
    "it'd've": "it would have",
    "it'll": "it will",
    "it'll've": "iit will have",
    "it's": "it is",
    "let's": "let us",
    "ma'am": "madam",
    "mayn't": "may not",
    "might've": "might have",
    "mightn't": "might not",
    "mightn't've": "might not have",
    "must've": "must have",
    "mustn't": "must not",
    "mustn't've": "must not have",
    "needn't": "need not",
    "needn't've": "need not have",
    "o'clock": "of the clock",
    "oughtn't": "ought not",
    "oughtn't've": "ought not have",
    "shan't": "shall not",
    "sha'n't": "shall not",
    "shan't've": "shall not have",
    "she'd": "she had",
    "she'd've": "she would have",
    "she'll": "she will",
    "she'll've": "she will have",
    "she's": "she is",
    "should've": "should have",
    "shouldn't": "should not",
    "shouldn't've": "should not have",
    "so've": "so have",
    "so's": "so is",
    "that'd": "that had",
    "that'd've": "that would have",
    "that's": "that is",
    "there'd": "there had",
    "there'd've": "there would have",
    "there's": "there is",
    "they'd": "they had",
    "they'd've": "they would have",
    "they'll": "they will",
    "they'll've": "they will have",
    "they're": "they are",
    "they've": "they have",
    "to've": "to have",
    "wasn't": "was not",
    "we'd": "we had",
    "we'd've": "we would have",
    "we'll": "we will",
    "we'll've": "we will have",
    "we're": "we are",
    "we've": "we have",
    "weren't": "were not",
    "what'll": "what will",
    "what'll've": "what will have",
    "what're": "what are",
    "what's": "what is",
    "what've": "what have",
    "when's": "when is",
    "when've": "when have",
    "where'd": "where did",
    "where's": "where is",
    "where've": "where have",
    "who'll": "who will",
    "who'll've": "who will have",
    "who's": "who is",
    "who've": "who have",
    "why's": "why is",
    "why've": "why have",
    "will've": "will have",
    "won't": "will not",
    "won't've": "will not have",
    "would've": "would have",
    "wouldn't": "would not",
    "wouldn't've": "would not have",
    "y'all": "you all",
    "y'all'd": "you all would",
    "y'all'd've": "you all would have",
    "y'all're": "you all are",
    "y'all've": "you all have",
    "you'd": "you had",
    "you'd've": "you would have",
    "you'll": "you will",
    "you'll've": "you will have",
    "you're": "you are",
    "you've": "you have"
}


def expand_contractions(text, contractions_dict):
    contractions_pattern = re.compile('({})'.format('|'.join(contractions_dict.keys())),
                                      flags=re.IGNORECASE | re.DOTALL)

    def expand_match(contraction):
        match = contraction.group(0)
        first_char = match[0]
        expanded_contraction = contractions_dict.get(match) \
            if contractions_dict.get(match) \
            else contractions_dict.get(match.lower())
        expanded_contraction = expanded_contraction
        return expanded_contraction

    expanded_text = contractions_pattern.sub(expand_match, text)
    expanded_text = re.sub("'", "", expanded_text)
    return expanded_text


def cons(text):
    text = expand_contractions(text, contractions_dict)
    return text


main_text = main_text.apply(cons)


# To lowercase
def to_lower(text):
    return ' '.join([w.lower() for w in word_tokenize(text)])


main_text = main_text.apply(to_lower)


# print(main_text)

# Remove Special Character and Punctuation

# Define function for removing special characters
def remove_special_characters(text, remove_digits=True):
    pattern = r'[^a-zA-z0-9\s]'
    text = re.sub(pattern, '', text)
    return text


def strip_punctuation(s):
    return ''.join(c for c in s if c not in punctuation)


main_text = main_text.apply(remove_special_characters)
main_text = main_text.apply(strip_punctuation)


# print(main_text)

# Replace Elongated Words loooooong = long  import wordnet
def replaceElongated(word):
    repeat_regexp = re.compile(r'(\w*)(\w)\2(\w*)')
    repl = r'\1\2\3'
    if wordnet.synsets(word):
        return word
    repl_word = repeat_regexp.sub(repl, word)
    if repl_word != word:
        return replaceElongated(repl_word)
    else:
        return repl_word


main_text = main_text.apply(replaceElongated)
# print(main_text)

# Tokenization
tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')

main_text = main_text.apply(lambda x: tokenizer.tokenize(x))


# print(main_text)

# Removing Stopwords
def remove_stopwords(text):
    words = [w for w in text if w not in stopword_list]
    return words


main_text = main_text.apply(lambda x: remove_stopwords(x))
# print(main_text)

# Stemming import SnowballStemmer
snowball_stemmer = SnowballStemmer('english')


def stem_update(text_list):
    text_list_new = []
    for word in text_list:
        word = snowball_stemmer.stem(word)
        text_list_new.append(word)
    return text_list_new


main_text = main_text.apply(stem_update)


# print(main_text)

# Drop Numbers
def drop_numbers(list_text):
    list_text_new = []
    for i in list_text:
        if not re.search('\d', i):
            list_text_new.append(i)
    return ' '.join(list_text_new)


main_text = main_text.apply(drop_numbers)
df = pd.concat([main_text, target], axis=1)
# print(df.head(2))

# Most Common Words in positive Text
plt.figure(figsize=(15, 15))  # Text that is recommended
wc = WordCloud(width=1600, height=800, max_words=3000).generate(" ".join(df[df.Sentiment == 'positive'].SentimentText))
plt.imshow(wc, interpolation='bilinear')
# plt.title("Positive")
# plt.show()

# Most Common Words in negative text
plt.figure(figsize=(15, 15))  # Text that is not recommended
wc = WordCloud(width=1600, height=800, max_words=3000).generate(" ".join(df[df.Sentiment == 'negative'].SentimentText))
plt.imshow(wc, interpolation='bilinear')
# plt.title("Negative")
# plt.show()

# Modelling using Multinomial Naive Bayes Split the data and count the vectorize in each words
cv = CountVectorizer()

train_data, test_data = train_test_split(df, train_size=0.8, random_state=0)

X_train = cv.fit_transform(train_data['SentimentText'])
y_train = train_data['Sentiment']
X_test = cv.transform(test_data['SentimentText'])
y_test = test_data['Sentiment']
# print(X_train.toarray())
# print(X_train)
# print(X_train)  # Print the sparse matrix representation
# print(X_train.shape)  # Print the shape of the sparse matrix
# print(X_train.nnz)  # Print the number of non-zero entries in the sparse matrix

nb = MultinomialNB()
nb.fit(X_train, y_train)
nb_predict = nb.predict(X_test)
# find accuracy
nb_report = accuracy_score(y_test, nb_predict)
print('Accuracy:', nb_report * 100)

nb_report1 = recall_score(y_test, nb_predict, pos_label='positive', average='binary')
print('Recall:', nb_report1 * 100)

nb_report2 = precision_score(y_test, nb_predict, pos_label='positive', average='binary')
print('Precision:', nb_report2 * 100)

nb_report2 = f1_score(y_test, nb_predict, pos_label='positive', average='binary')
print('f1_score:', nb_report2 * 100)

# Predict sentiment using the trained model
new_statement = "This is not a product"  # Replace this with the statement you want to classify
new_statement = cons(new_statement)  # Expand contractions
new_statement = to_lower(new_statement)  # Convert to lowercase
new_statement = remove_special_characters(new_statement)  # Remove special characters
new_statement = strip_punctuation(new_statement)  # Remove punctuation
new_statement = replaceElongated(new_statement)  # Replace elongated words
new_statement_tokens = tokenizer.tokenize(new_statement)  # Tokenization
new_statement_tokens = remove_stopwords(new_statement_tokens)  # Remove stopwords
new_statement_tokens = stem_update(new_statement_tokens)  # Apply stemming
new_statement_cleaned = drop_numbers(new_statement_tokens)  # Remove numbers
new_statement_cleaned = ' '.join(new_statement_cleaned)  # Convert list of tokens back to string
new_statement_vectorized = cv.transform([new_statement_cleaned])  # Vectorize the new statement

predicted_sentiment = nb.predict(new_statement_vectorized)
if predicted_sentiment[0] == 'positive':
    print("The given statement is positive.")
else:
    print("The given statement is negative.")
