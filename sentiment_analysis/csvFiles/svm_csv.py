import warnings
from nltk import ngrams
from collections import Counter
from sklearn.feature_extraction.text import TfidfVectorizer

warnings.filterwarnings("ignore", category=DeprecationWarning)
import pandas as pd
import re, nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
# preparing the data
import numpy as np
from scipy.sparse import hstack
from sklearn.feature_extraction.text import CountVectorizer
# data split
from sklearn.model_selection import train_test_split
# find accuracy
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder

csv_path = r'C:\Users\admin\PycharmProjects\pythonTask\sentiment_analysis\csvFiles\tweet.csv'
df = pd.read_csv(csv_path, delimiter=',', encoding='ISO-8859-1', low_memory=False)
col = list(df.columns.values)
print(df.head())

# what sentiments have been found.
sentiment_counts = df.label.value_counts()
number_of_tweets = df.id.count()
print(sentiment_counts)

# Preprocessing¶
stop_words = set(stopwords.words('english'))
wordnet_lemmatizer = WordNetLemmatizer()
# positive = df.label == '1'
# print(positive)
# negative = df.label == '0'
# all = positive + negative

# def normalizer(tweet):
#     only_letters = re.sub("[^a-zA-Z]", " ", tweet)
#     tokens = nltk.word_tokenize(only_letters)[2:]
#     lower_case = [l.lower() for l in tokens]
#     filtered_result = list(filter(lambda l: l not in stop_words, lower_case))
#     lemmas = [wordnet_lemmatizer.lemmatize(t) for t in filtered_result]
#     return lemmas

def normalizer(tweet):
    only_letters = re.sub("[^a-zA-Z]", " ", tweet)
    tokens = nltk.word_tokenize(only_letters)
    lower_case = [l.lower() if idx >= 2 else l for idx, l in enumerate(tokens)]
    filtered_result = list(filter(lambda l: l not in stop_words, lower_case))
    lemmas = [wordnet_lemmatizer.lemmatize(t) for t in filtered_result]
    return lemmas

pd.set_option('display.max_colwidth', None)  # Let pandas determine the display width

df['ProcessedText'] = df.tweet.apply(normalizer)

dff = df[['tweet', 'ProcessedText']].head()


# print(dff)
def ngrams(input_list):
    # onegrams = input_list
    bigrams = [' '.join(t) for t in list(zip(input_list, input_list[1:]))]
    trigrams = [' '.join(t) for t in list(zip(input_list, input_list[1:], input_list[2:]))]
    return bigrams + trigrams


df['grams'] = df.tweet.apply(ngrams)
df1 = df[['grams']].head()

print(df1)


#
# def count_words(input):
#     cnt = Counter()
#     for row in input:
#         cnt.update(row)
#     return cnt

def count_words(input):
    cnt = Counter()
    for row in input:
        if isinstance(row, str):
            cnt.update([row.lower()])
        else:
            cnt.update(row)
    return cnt

#
negative_rows = df[df['label'] == 'positive']
print(negative_rows.head())  # Print the first few rows with negative sentiment
com = negative_rows['grams'].apply(count_words)

combined_counter = Counter()
for counter in com:
    combined_counter += counter

com_most_common = combined_counter.most_common(5)
print(com_most_common)
#
# # Preparing the data
count_vectorizer = CountVectorizer(ngram_range=(1, 2))
vectorized_data = count_vectorizer.fit_transform(df.tweet)
indexed_data = hstack((np.array(range(0, vectorized_data.shape[0]))[:, None], vectorized_data))


def sentiment2target(sentiment):
    return {
        'negative': 0,
        'positive': 1
    }.get(sentiment,None)


targets = df.tweet.apply(sentiment2target)
#
# # split our data in to train and test
#
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(df.ProcessedText)

X_train, X_test, y_train, y_test = train_test_split(X, labels, test_size=0.2, random_state=42)
svm_classifier = SVC(kernel='linear')
svm_classifier.fit(X_train, y_train)

y_pred = svm_classifier.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
print("Accuracy:", accuracy)