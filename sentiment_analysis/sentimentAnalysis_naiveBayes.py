#USE POSTAG
from nltk.tag import pos_tag
# IMPORT "twitter_samples" DATASET
from nltk.corpus import twitter_samples
#word lemmatization
from nltk.stem.wordnet import WordNetLemmatizer
import re, string
from nltk.corpus import stopwords
from nltk import FreqDist
import random
# Accuracy
from nltk import classify
from nltk import NaiveBayesClassifier
# CHECK MODEL PERFORMANCE
from nltk.tokenize import word_tokenize
import random

# EXTRACT THE POSITIVE, NEGATIVE AND NUTRAL DATASET

positive_tweets = twitter_samples.strings('positive_tweets.json')
negative_tweets = twitter_samples.strings('negative_tweets.json')
text = twitter_samples.strings('tweets.20150430-223406.json')

#TOKENIZE THE DATASETS
tweet_tokens = twitter_samples.tokenized('positive_tweets.json')
positive_tweet_tokens = twitter_samples.tokenized('positive_tweets.json')
negative_tweet_tokens = twitter_samples.tokenized('negative_tweets.json')

# stop_words = stopwords.words('english')
# print(remove_noise(tweet_tokens[0], stop_words))


positive_cleaned_tokens_list = []
negative_cleaned_tokens_list = []

#USING LEMMATIZATION TO REMOVING NOISE LIKE URLS(http,https) AND USERNAME
def remove_noise(tweet_tokens, stop_words=()):
    cleaned_tokens = []

    for token, tag in pos_tag(tweet_tokens):
        token = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+#]|[!*\(\),]|' \
                       '(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', token)
        token = re.sub("(@[A-Za-z0-9_]+)", "", token)

        if tag.startswith("NN"):
            pos = 'n'
        elif tag.startswith('VB'):
            pos = 'v'
        else:
            pos = 'a'

        lemmatizer = WordNetLemmatizer()
        token = lemmatizer.lemmatize(token, pos)

        if len(token) > 0 and token not in string.punctuation and token.lower() not in stop_words:
            cleaned_tokens.append(token.lower())
    return cleaned_tokens


stop_words = stopwords.words('english')
# print(remove_noise(tweet_tokens[0], stop_words))
for tokens in positive_tweet_tokens:
    positive_cleaned_tokens_list.append(remove_noise(tokens, stop_words))

for tokens in negative_tweet_tokens:
    negative_cleaned_tokens_list.append(remove_noise(tokens, stop_words))


# print(remove_noise(tweet_tokens[0], stop_words))
# print(positive_tweet_tokens[500])
# print(positive_cleaned_tokens_list[500])

#Determine word density that is how many time that word is occurres in that data set
def get_all_words(positive_cleaned_tokens):
    for tokens in positive_cleaned_tokens:
        for token in tokens:
            yield token


all_pos_words = get_all_words(positive_cleaned_tokens_list)
freq_dist_pos = FreqDist(all_pos_words)
print(freq_dist_pos.most_common(10))

#Converting Tokens to a Dictionary
def get_tweets_for_model(cleaned_tokens_list):
    for tweet_tokens in cleaned_tokens_list:
        yield dict([token, True] for token in tweet_tokens)



#POST TAG OF THE INDEX 0 TH POSITIVE
tweet_tokens = twitter_samples.tokenized('positive_tweets.json')
print(pos_tag(tweet_tokens[0]))


positive_tokens_for_model = get_tweets_for_model(positive_cleaned_tokens_list)
negative_tokens_for_model = get_tweets_for_model(negative_cleaned_tokens_list)

positive_dataset = [(tweet_dict, "Positive")
                    for tweet_dict in positive_tokens_for_model]
# print(positive_dataset)
negative_dataset = [(tweet_dict, "Negative")
                    for tweet_dict in negative_tokens_for_model]
# print(positive_dataset)
dataset = positive_dataset + negative_dataset

random.shuffle(dataset)

train_data = dataset[:7000]
test_data = dataset[7000:]

# ACCURACY
classifier = NaiveBayesClassifier.train(train_data)
#FINDING ACCURACY
accuracy = classify.accuracy(classifier, test_data)
print("Accuracy is:", accuracy * 100, "%")


# Calculate precision
tp = 0  # True Positives
fp = 0  # False Positives

for features, label in test_data:
    predicted_label = classifier.classify(features)
    if predicted_label == label:
        tp += 1
    else:
        fp += 1

precision = tp / (tp + fp)
print("Precision is:", precision * 100, "%")

# Calculate recall
tp = 0  # True Positives
fn = 0  # False Negatives

for features, label in test_data:
    predicted_label = classifier.classify(features)
    if predicted_label == label == 'Positive':
        tp += 1
    elif predicted_label == 'Negative' and label == 'Positive':
        fn += 1

if tp + fn > 0:
    recall = tp / (tp + fn)
    print("Recall is:", recall * 100, "%")
else:
    print("No positive instances in the test set.")

# Calculate F1 score
if precision + recall > 0:
    f1_score = 2 * (precision * recall) / (precision + recall)
    print("F1 Score is:", f1_score * 100, "%")
else:
    print("No positive instances in the test set.")


custom_tweet = input("Enter feed back: ")

custom_tokens = remove_noise(word_tokenize(custom_tweet))

print(classifier.classify(dict([token, True] for token in custom_tokens)))

# print(classifier.classify(custom_tokens))

