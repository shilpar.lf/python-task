# import nltk
# nltk.download('movie_reviews')


from nltk.tag import pos_tag
from nltk.corpus import twitter_samples
from sklearn.svm import SVC
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
#_____FINDING ACCURACY_________
# from sklearn.metrics import accuracy_score
#______ FINDING PRECISION________
from sklearn.metrics import precision_score
#________FINDING RECALL______
from sklearn.metrics import recall_score

from nltk.tokenize import word_tokenize

# Tokenize the tweets and get the part-of-speech tags for the first tweet
tweet_tokens = twitter_samples.tokenized('positive_tweets.json')
pos_tags = pos_tag(tweet_tokens[0])
print(pos_tags)


# Function to convert tokenized tweets to a format suitable for SVM algorithm
def get_tweets_for_model(cleaned_tokens_list):
    for tweet_tokens in cleaned_tokens_list:
        yield ' '.join(tweet_tokens)


# Create a list of positive tweets in SVM algorithm format
positive_tweets = list(get_tweets_for_model(tweet_tokens))
positive_labels = ['positive'] * len(positive_tweets)

# Similarly, you can create a list of negative tweets and labels
negative_tokens = twitter_samples.tokenized('negative_tweets.json')
negative_tweets = list(get_tweets_for_model(negative_tokens))
negative_labels = ['negative'] * len(negative_tweets)

# Combine positive and negative tweets and labels
all_tweets = positive_tweets + negative_tweets
all_labels = positive_labels + negative_labels

# Vectorize the tweets using TfidfVectorizer
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(all_tweets)

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, all_labels, test_size=0.2, random_state=42)

# Create and train the SVM classifier
svm_classifier = SVC(kernel='linear')
svm_classifier.fit(X_train, y_train)

# Make predictions on the test set
y_pred = svm_classifier.predict(X_test)


#FINDING ACCURACY
# accuracy = accuracy_score(y_test, y_pred)
# print("Accuracy:", accuracy * 100 ,"%")

# FINDING PRECISION
# precision = precision_score(y_test, y_pred, pos_label='positive')
# print("Precision:", precision * 100, "%")

recall = recall_score(y_test, y_pred, pos_label='positive')
print("Recall:", recall * 100 ,"%")

def predict_sentiment(text):
    text_tokens = word_tokenize(text)
    text_vectorized = vectorizer.transform([' '.join(text_tokens)])
    sentiment = svm_classifier.predict(text_vectorized)[0]
    return sentiment


# Test the predict_sentiment function
text_to_predict = input("Enter your Command: ")
sentiment = predict_sentiment(text_to_predict)
print("Predicted Sentiment:", sentiment)
