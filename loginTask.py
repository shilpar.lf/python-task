from flask import Flask, render_template, request, redirect, session
from flask_mysqldb import MySQL

app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '1234'
app.config['MYSQL_DB'] = 'employee'

mysql = MySQL(app)


@app.route('/')
def hello_world():
    return render_template("login.html")


# database = {'shilpa': '123',
#             'bala': 'xyz',
#             'Thamayanthri': 'abc', 'lavanya': 'pqr'}


@app.route('/form_login', methods=['POST', 'GET'])
def login():
    username = request.form['username']
    password = request.form['password']
    cursor = mysql.connection.cursor()
    cursor.execute("SELECT * FROM users WHERE username = %s AND password = %s", (username, password))
    user = cursor.fetchone()

    if user:
        return render_template('home.html',
                               name=username)
    else:
        return render_template('login.html',
                               info='Invalid User ????!')


if __name__ == '__main__':
    app.run()
