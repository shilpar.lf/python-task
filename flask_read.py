# from flask import Flask, render_template
# import pandas as pd
#
# # *** Backend operation
# # Read csv file in python_ flask


from flask import Flask, render_template

import pandas as pd

app = Flask(__name__)

df = pd.read_csv('employee.csv')
df.to_csv('employee.csv', index=None)


@app.route('/')
@app.route('/table')
def table():
    data = pd.read_csv('employee.csv')
    return render_template('show_csv_data.html', tables=[data.to_html()], titles=[''])


if __name__ == "__main__":
    app.run(debug=True)
