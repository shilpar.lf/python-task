from flask import Flask, request, render_template
import csv
import io


app = Flask(__name__, template_folder='templates')


@app.route('/')
def index():
    return render_template('user.html')


@app.route('/upload', methods=['POST'])
def upload():
    if 'csv_file' not in request.files:
        return 'No file uploaded.'

    file = request.files['csv_file']
    if file.filename == '':
        return 'No file selected.'

    # Read CSV file
    csv_data = []
    csv_content = file.read().decode('utf-8').splitlines()
    reader = csv.reader(csv_content)
    for row in reader:
        csv_data.append(row)
    # with file:
    #     reader = csv.reader(files)
    #     for row in reader:
    #         csv_data.append(row)

    # Process the CSV data (e.g., perform calculations, manipulate data, etc.)
    # ...

    # Write CSV file
    output_file = 'output.csv'
    with open(output_file, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(csv_data)

    return f'CSV file uploaded and processed. Output saved as {output_file}.'
        # Write CSV file
    # output_file = io.StringIO()
    # writer = csv.writer(output_file)
    # writer.writerows(csv_data)
    #
    # output_file.seek(0)
    # return send_file(output_file, attachment_filename='output.csv', as_attachment=True)


if __name__ == '__main__':
    app.run(debug=True)
