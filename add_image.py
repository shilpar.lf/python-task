import mysql.connector

db_config = {
    "host": "localhost",
    "user": "root",
    "password": "1234",
    "database": "details",
}


def insert_image_into_table(image_path):
    try:
        connection = mysql.connector.connect(**db_config)
        cursor = connection.cursor()

        with open(image_path, "rb") as image_file:
            image_data = image_file.read()

        sql = "INSERT INTO employees (employee_id, first_name, last_name, employee_post, salary, img) VALUES (%s, %s, " \
              "%s, %s, %s, %s)"
        values = (5, 'pushbaraj', 'A', 'Android', 25000, image_data)
        cursor.execute(sql, values)

        connection.commit()
        print("Image inserted successfully.")

    except mysql.connector.Error as error:
        print("Error:", error)

    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()


insert_image_into_table(r'C:\Users\admin\Downloads\5p.jpg')
