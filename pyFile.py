from flask import Flask, request, render_template
import csv

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

# @app.route('/upload', methods=['POST'])
# def upload():
#     if 'csv_file' not in request.files:
#         return 'No file uploaded.'
#
#     file = request.files['csv_file']
#     if file.filename == '':
#         return 'No file selected.'
#
#     # Read CSV file
#     csv_data = []
#     with file:
#         reader = csv.reader(file)
#         for row in reader:
#             csv_data.append(row)
#
#     # Process the CSV data (e.g., perform calculations, manipulate data, etc.)
#     # ...
#
#     # Write CSV file
#     output_file = 'output.csv'
#     with open(output_file, 'w', newline='') as file:
#         writer = csv.writer(file)
#         writer.writerows(csv_data)
#
#     return f'CSV file uploaded and processed. Output saved as {output_file}.'

if __name__ == '__main__':
    app.run()
