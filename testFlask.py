from flask import Flask

app = Flask(__name__)  # create object for flask


@app.route('/')
def hello_world() -> object:
    return '<h1>Hello Logic Focus</h1> <p> Learn More Be Smart</p>'


if __name__ == '__main__':
    app.run(debug=True)




# # Read excel file in python_ flask
# # df = pd.read_excel('data/comment.xlsx')
#
# # WSGI Application
# # Configure template folder name
# # The default folder name should be "templates" else need to mention custom folder name for template path
# # The default folder name for static files should be "static" else need to mention custom folder for static path
# app = Flask(__name__, template_folder='templates')
#
#
# @app.route('/')
# def index():
#     return render_template('index.html')
#
#
# @app.route('/show_data', methods=("POST", "GET"))
# def showData():
#     # Convert pandas dataframe to html table flask
#     df_html = df.to_html()
#     return render_template('show_csv_data.html', data=df_html)
#     return render_template('show_csv_data.html', tables=[data.to_html()], titles=[''])
#
#
#
# if __name__ == '__main__':
#     app.run(debug=True)
