import os
import numpy as np
import cv2
import imutils
import sys
import pytesseract
import logging


log_filename = os.path.join(os.getcwd(), 'npr.log')
logging.basicConfig(filename=log_filename, filemode='a', level=logging.DEBUG, format='%(asctime)s-%(levelname)s:%(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')
logger = logging.getLogger("npr")


def image_process(input_image, width=500, psm=3):
    logger.info("Image: %s, width: %s, psm: %s", input_image, width, psm)
    image = cv2.imread(input_image)
    image = imutils.resize(image, width)  # 600, 500
    # cv2.imshow("Original Image", image)

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # cv2.imshow("1 - Grayscale Conversion", gray)

    gray = cv2.bilateralFilter(gray, 11, 17, 17)
    # cv2.imshow("2 - Bilateral Filter", gray)

    edged = cv2.Canny(gray, 170, 200)
    cv2.imshow("4 - Canny Edges", edged)

    counts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[0]
    counts = sorted(counts, key=cv2.contourArea, reverse=True)[:30]
    number_plate_cnt = None

    for c in counts:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)
        if len(approx) == 4:
            number_plate_cnt = approx
            break

    if number_plate_cnt is not None:
        # Masking the part other than the number plate
        mask = np.zeros(gray.shape, np.uint8)
        new_image = cv2.drawContours(mask, [number_plate_cnt], 0, 255, -1)
        new_image = cv2.bitwise_and(image, image, mask=mask)

        # Configuration for tesseract
        config = ('-l eng --oem 1 --psm {psm}'.format(psm=psm))

        # Run tesseract OCR on image
        text = pytesseract.image_to_string(new_image, config=config)

        # return recognized text
        return text

    return ""


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python script_name.py input_image_path")
        sys.exit(1)

    input_img = sys.argv[1]
    npr = image_process(input_img)
    if npr == "":
        npr = image_process(input_img, width=600)
    if npr == "":
        npr = image_process(input_img, psm=7)
    print(npr)






