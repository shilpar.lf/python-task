import mysql.connector

# Establish a connection
conn = mysql.connector.connect(
    host='localhost',
    user='root',
    password='1234',
    database='employee'
)

# Create a cursor
cursor = conn.cursor()

# Execute a query
cursor.execute("SELECT * FROM details WHERE emp_id=1")

# Fetch the result
rows = cursor.fetchall()

# Print the result
for row in rows:
    print(row)

# Close the cursor and connection
cursor.close()
conn.close()
