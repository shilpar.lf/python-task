from flask import Flask, render_template, request, redirect, url_for
import mysql.connector
import base64

app = Flask(__name__)

db_config = {
    'host': 'localhost',
    'user': 'root',
    'password': '1234',
    'database': 'employee',
}


def get_db_connection():
    return mysql.connector.connect(**db_config)


@app.route('/', methods=['GET'])
def index():
    return render_template('image_upload.html')


@app.route('/upload', methods=['POST'])
def upload_image():
    message =" "
    image_base64 = None
    if request.method == 'POST':

        try:
            if 'image' not in request.files:
                return "No image uploaded", 400

            image_file = request.files['image']
            if image_file.filename == '':
                return "No selected image", 400

            connection = get_db_connection()
            cursor = connection.cursor()

            image_content = image_file.read()

            query = "INSERT INTO sample (img) VALUES (%s)"
            cursor.execute(query, (image_content,))
            connection.commit()
            cursor.close()
            connection.close()
            # return "Image uploaded successfully"
            message = "Image uploaded successfully!!!!..."
            image_base64 = base64.b64encode(image_content).decode('utf-8')




        except Exception as e:
            return str(e), 500

    return render_template('image_upload.html', message=message, image_base64=image_base64)


@app.route('/image/<int:image_id>', methods=['GET'])
def get_image(image_id):
    try:
        connection = get_db_connection()
        cursor = connection.cursor()

        query = "SELECT img FROM sample WHERE id = %s"
        cursor.execute(query, (image_id,))
        image_data = cursor.fetchone()

        if not image_data:
            return "Image not found", 404

        cursor.close()
        connection.close()

        image_blob = image_data[0]
        image_base64 = base64.b64encode(image_blob).decode('utf-8')

        return render_template('image_display.html', image_base64=image_base64)

    except Exception as e:
        return str(e), 500


if __name__ == '__main__':
    app.run(debug=True)
